import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;

public class SortTest {
    /** maximal array length */
    static final int MAX_SIZE = 10;

    private static DecimalFormat df2 = new DecimalFormat("#.##");

    public static void main(String[] args) {
        final double[] origArray = new double[MAX_SIZE];
        Random generator = new Random();
        for (int i = 0; i < MAX_SIZE; i++) {
            double randomDouble = generator.nextDouble()*1000.;
            origArray[i] = Double.parseDouble(df2.format(randomDouble));
        }
        double[] origArray2 = {1.0, 4.0, 2.0, 1.0, 6.0, 3.0, 6.0, 8.0, 6.0, 6.0};
        System.out.println("Orig array: " + Arrays.toString(origArray2));
        binaryInsertionSort(origArray2);
    }
    private static void binaryInsertionSort(double[] a) {
        for (int cursorIndex = 0; cursorIndex < a.length; cursorIndex++) { // loop through all array positions
            double el_toSort = a[cursorIndex]; // next element to sort
            System.out.println("----------> cursorIndex " + cursorIndex + ": " + el_toSort); // DEBUG
            int cursorCycleFirstIndex = 0; // first index behind cursor index
            int cursorCycleLastIndex = cursorIndex - 1; // last index behind cursor index
            int tries = 0;
            while (true) { // loop binary search in range of indexes behind cursor (cursor cycle) until position is found for element-to-be-sorted
                tries++;
                if (tries == 15){ // ANTI forever-loop (for development)
                    break;
                }
                int cursorCycleIndexes = cursorCycleLastIndex - cursorCycleFirstIndex;
                int cutIndex = (cursorCycleFirstIndex + cursorCycleLastIndex) / 2; // finding the center element index of the cursor cycle
                System.out.println("cutIndex " + cutIndex + ": " + a[cutIndex] + " ( " + cursorCycleFirstIndex + " + " +  cursorCycleLastIndex + " / 2 )");
                if (cutIndex == cursorIndex) { // if the element is already in the right position then do nothing
                    System.out.println("already in position");
                    break;
                } else {
                    double el_toCompare = a[cutIndex]; // center element of the cursor cycle
                    if (el_toSort == el_toCompare) { // if element-to-be-sorted is equal to cursor cycle center element
                        // put element in there
                        System.out.println("equals");
                        System.arraycopy(a, cutIndex + 1, a, cutIndex + 2, cursorIndex - cutIndex - 1); // (srcArray, srcInt, destArray, destInt, length)
                        a[cutIndex + 1] = el_toSort;
                        System.out.println("a: " + Arrays.toString(a));
                        break;
                    } else if (el_toSort > el_toCompare) { // if element-to-be-sorted is greater than cursor cycle center element
                        // next cursor cycle = center element index til current cursor cycle end index
                        cursorCycleFirstIndex = cutIndex + 1; // change only cursor cycle first index because last index stays default
                        System.out.println("greater");
                        if (cursorCycleIndexes <= 0){ // if only 1 element left in cursor cycle then put element there
                            System.out.println("only 1 left in cursor cycle, let's put there");
                            System.arraycopy(a, cutIndex + 1, a, cutIndex + 2, cursorIndex - cutIndex - 1); // (srcArray, srcInt, destArray, destInt, length)
                            a[cutIndex + 1] = el_toSort;
                            System.out.println("a: " + Arrays.toString(a));
                            break;
                        }
                    } else if (el_toSort < el_toCompare) { // if element-to-be-sorted is less than cursor cycle center element
                        // next cursor cycle = current cursor cycle start index til center element index
                        cursorCycleLastIndex = cutIndex - 1; // change only cursor cycle last index because first index stays default
                        System.out.println("less");
                        if (cursorCycleIndexes <= 0){ // if only 1 element left in cursor cycle then put element there
                            System.out.println("only 1 left in cursor cycle, let's put there");
                            System.arraycopy(a, cutIndex, a, cutIndex + 1, cursorIndex - cutIndex); // (srcArray, srcPos, destArray, destPos, length)
                            a[cutIndex] = el_toSort;
                            System.out.println("a: " + Arrays.toString(a));
                            break;
                        }
                    }
                }
            }
        }
    }
}
